module NestHelper
  module ErrorHelper
    protected

    #
    # returns the string for an error on the `field` of `object`, if any.
    #
    # Object can be a Hash, or an ActiveRecord model.
    #
    # There are two types of error strings:
    #
    # * Full messages: these have the attriute name prefixed in front (depending on locale)
    # * Raw Messages: no attribute prefix
    #
    # To force raw message, set full_message: false in options
    #
    def error_str(object, field=nil, options={})
      object = object.errors if object.respond_to?(:errors)
      str = if field.present?
        if object.respond_to?(:full_messages_for) && options[:full_message]
          object.full_messages_for(field)
        elsif object.respond_to?(:[])
          object[field]
        end
      else
        object
      end
      str = format_error_msg(str)
      if str.present?
        return str
      else
        return nil
      end
    end

    #
    # returns the string 'is-invalid' if thing contains errors, and
    # an empty string otherwise.
    #
    def is_invalid(object, field=nil)
      error_str(object, field).present? ? 'is-invalid' : ''
    end

    def model_errors(model, messages: false)
      if model.errors.any?
        render partial: 'errors/model_errors', locals: {model: model, messages: messages}
      end
    end

    def has_error?(object, field=nil)
      error_str(object, field).present?
    end

    def feedback(model, field=nil, options={})
      options[:error_class] ||= 'invalid-feedback'
      options[:full_message] = true if options[:full_message].nil?
      if field.nil?
        model_errors(model)
      else
        str = error_str(model, field, options)
        if str.present? && str != true
          content_tag :div, str, class: [options[:error_class], options[:class]].compact.join(' ')
        end
      end
    end

    #
    # same as feedback() but ensures that the error message is always visible.
    # with the standard 'invalid-feedback' css class, the message is only visible
    # if the related input field also is flagged as invalid
    #
    def general_feedback(model, field=nil, options={})
      options[:error_class] ||= 'always-visible-invalid-feedback'
      feedback(model, field, options)
    end

    #
    # Generates HTML markup from flash.
    #
    # Items may be a String, Array, Exception, or ActiveModel::Errors
    #
    def flash_messages(messages)
      if messages.nil? || (messages.respond_to?(:empty?) && messages.empty?)
        return nil
      end

      bullets = []
      messages = [messages].flatten

      messages.each do |msg|
        if msg.is_a?(String)
          bullets << safe_format(msg)
        elsif msg.respond_to?(:errors)
          msg.errors.full_messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.respond_to?(:full_messages)
          msg.full_messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.is_a?(ErrorMessage)
          msg.messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.is_a?(StandardError)
          bullets << safe_format(msg.to_s)
        elsif msg.is_a?(Array) && msg.any?
          msg.each do |i|
            bullets << safe_format(i)
          end
        end
      end

      if bullets.size == 1
        return bullets.first
      else
        return content_tag(:ul) {
          bullets.map{|b| content_tag(:li, b)}.join("\n").html_safe
        }
      end
    end

    private

    #
    # error messages might be arrays or strings or...
    #
    def format_error_msg(msg)
      if msg === true
        true
      elsif msg.respond_to?(:join)
        msg.join(". ").gsub('..','.')
      else
        msg.to_s
      end
    end

    SAFE_FORMAT_MAP = {
      "[b]"  => "<b>",
      "[/b]" => "</b>",
      "[r]"  => "<span style='color:red; font-weight: bold'>",
      "[/r]" => "</span>"
    }

    SAFE_FORMAT_RE = Regexp.union(SAFE_FORMAT_MAP.keys)

    def safe_format(str)
      h(str).gsub(SAFE_FORMAT_RE, SAFE_FORMAT_MAP).html_safe
    end

  end
end