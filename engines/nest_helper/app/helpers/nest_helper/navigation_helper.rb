module NestHelper
  module NavigationHelper
    protected

    #
    # returns the string 'active' if the arguments match against the params object.
    #
    # Two argument forms:
    #
    # * hash:  active if every condition in the hash is true.
    #          each key is matched against params.
    #          the value can be a RegExp or an array (which matches if any items in array match).
    #
    # * array: each element is handled as a hash and active is returned
    #          if and only if the conditions match for all hashes.
    #
    # You can use the aliases :c and :a instead of :controller and :action.
    #
    # examples:
    #
    #   active(controller: 'home', action: 'show')
    #   active(c: 'home', a: 'show')
    #   active({controller: 'animals', action: ['index', 'show']}, {view: /animals/})
    #
    def active(*args)
      raise ArgumentError, 'action() expects a hash or array of hashes' unless args[0].is_a?(Hash)
      args.each do |hsh|
        is_active = hsh.keys.inject(true) {|memo,key|
          if key == :c
            pkey = :controller
          elsif key == :a
            pkey = :action
          else
            pkey = key
          end
          memo &&= active_match_value(hsh[key], params[pkey])
        }
        if is_active
          return 'active'
        end
      end
      # no matches found:
      return nil
    end

    def nav_tab(label, path, klass, disabled: false)
      content_tag('li', class: 'nav-item') do
        link_to(label, path, class: ['nav-link', klass, disabled ? 'disabled' : nil].compact.join(' '))
      end
    end

    private

    def active_match_value(value, param)
      if value.is_a? Regexp
        !!(value =~ param)
      elsif value.is_a? Array
        value.inject(true) {|memo, i| memo &&= active_match_value(i, param) }
      else
        value == param
      end
    end

  end
end