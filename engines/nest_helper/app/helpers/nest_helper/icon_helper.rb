module NestHelper
  module IconHelper
    protected

    ICON_MAP = {
      cancel: 'x-lg',
      ok: 'check-lg',
      next: 'chevron-double-right',
      prev: 'chevron-double-left',
      plus: 'plus-lg'
    }

    #
    # accepted options:
    #
    # class:
    # fill:
    # height:
    # width:
    #
    def icon(*args, &block)
      symbol, label, options = parse_icon_arguments(*args, &block)
      svg = icon_svg(symbol, options)
      if label
        label = ERB::Util.html_escape_once(label) unless label.html_safe?
        "#{svg} #{label}".html_safe
      else
        svg
      end
    end

    def icon_right(*args, &block)
      symbol, label, options = parse_icon_arguments(*args, &block)
      svg = icon_svg(symbol, options)
      if label
        label = ERB::Util.html_escape_once(label) unless label.html_safe?
        "#{label} #{svg}".html_safe
      else
        svg
      end
    end

    private

    def icon_svg(symbol, options)
      if defined?(bootstrap_icon)
        bootstrap_icon(symbol, options||{})
      end
    end

    def parse_icon_arguments(*args, &block)
      symbol  = symbol_map(args.first)
      label   = nil
      options = nil
      if args[1].is_a? String
        label   = args[1]
        options = args[2]
      else
        options = args[1]
      end
      if block_given?
        label = yield
      end
      return [symbol, label, options]
    end

    def symbol_map(symbol)
      ICON_MAP[symbol] || symbol.to_s
    end
  end
end
