module NestHelper
  module MarkdownHelper

    def markdown(str)
      if defined?(Redcarpet)
        Redcarpet::Markdown.new(
          Redcarpet::Render::HTML.new(
            filter_html: true, no_images: true,
            no_styles: true, safe_links_only: true
          ),
          autolink: true
        ).render(str).html_safe
      elsif defined?(Kramdown)
        Kramdown::Document.new(str).to_html.html_safe
      elsif defined?(Rdiscount)
        RDiscount.new(str).to_html.html_safe
      elsif defined?(Markuku)
        Maruku.new(str).to_html.haml_safe
      else
        str
      end
    end

    #
    # convert markdown to look a bit more like ordinary plain text:
    #
    # 1. Convert the links in markdown to more friendly footnote links
    # 2. Remove inline formatting
    # 3. Ensure no double returns
    #
    def markdown_to_text(str, base_url: nil)
      str = str.dup

      # convert links to footnotes
      regexp = Regexp.new(/\[([^\]]+)\]\(([^\)]+)\)/)
      current_footnote = 0
      footnotes = []
      str.gsub!(regexp) do |link|
        match = link.match(regexp)
        text = match[1]
        url = normalize_url(path: match[2], base: base_url)
        this_footnote = current_footnote
        current_footnote += 1
        footnotes[this_footnote] = url
        "#{text} [#{this_footnote}]"
      end
      if footnotes.any?
        str.concat("\n\n")
        footnotes.each_with_index do |url, i|
           str.concat("[#{i}] #{url}\n")
        end
      end

      # remove inline formatting
      str.gsub!(/(\*\*+|__+)/, '')

      # remove double returns
      str.gsub!(/\n\n\n+/, "\n\n")

      return str
    end

    def normalize_url(base:nil, path:nil)
      base ||= ""
      path ||= ""
      if path =~ /^\//
        if base != "" && base !~ /^http/
          if base =~ /localhost/
            base = "http://" + base
          else
            base = "https://" + base
          end
        end
        url = [base, path].join('/')
      else
        url = path
      end
      url = url.gsub(/(([^:])\/\/+)/, '\2/') # remove duplicate /
      return url
    end

  end
end