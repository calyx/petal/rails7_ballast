$:.push File.expand_path("lib", __dir__)

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "nest_helper"
  spec.version     = "0.1.0"
  spec.authors     = ["elijah"]
  spec.homepage    = "https://0xacab.org/calyx/gems/nest_helper"
  spec.summary     = "Useful helpers for Ruby on Rails"
  spec.license     = "MIT"

  spec.files = Dir["{app,lib}/**/*", "LICENSE", "README.md"]
end
