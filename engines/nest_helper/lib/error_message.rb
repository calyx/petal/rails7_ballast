#
# A simple exception that holds one or more messages.
#
# Usage:
#
#   raise ErrorMessage, "bad thing"
#
#   raise ErrorMessage, ["a bad thing", "yet another bad thing"]
#
class ErrorMessage < StandardError
  attr_accessor :tag
  attr_accessor :messages

  def message
    messages.join('. ')
  end

  def to_s
    message
  end

  def initialize(msg=nil, tag: "")
    super(nil)
    if msg.is_a? String
      @messages = [msg]
    elsif msg.is_a? Array
      @messages = msg
    end
    @tag = tag
  end
end