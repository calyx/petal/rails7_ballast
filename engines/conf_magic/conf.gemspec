Gem::Specification.new do |spec|
  spec.name        = "conf_magic"
  spec.version     = "0.1.0"
  spec.authors     = ["elijah"]
  spec.homepage    = "https://0xacab.org/calyx/gems/conf"
  spec.summary     = "Configuration toolkit for Ruby on Rails"
  spec.license     = "MIT"
  spec.files = Dir["lib/**/*", "README.md"]
end
