class Conf
  @loaded = false

  def self.method_missing(method, *args)
    unless @loaded
      raise ArgumentError, 'Conf.load not yet called'
    end
    @data[method.to_s] || args.first
  end

  def self.load(*paths)
    current_path = nil
    @data ||= {}
    @data = paths.inject(@data) {|config, filepath|
      current_path = filepath
      if File.exist?(filepath) && settings = load_yaml_file(filepath)
        if settings && settings[Rails.env]
          config.merge!(settings[Rails.env])
        else
          config
        end
      else
        config
      end
    }
    unless @data.any?
      raise StandardError, "ERROR Could not load any configuration in paths #{paths.join(', ')}"
    end
    @loaded = true
  rescue Exception => exc
    puts "ERROR parsing configuration file (#{current_path})."
    puts exc
    puts exc.callstack if exc.respond_to?(:callstack)
    exit 1
  end

  def self.data
    @data
  end

  #
  # creates a temporary stub for a configuration value.
  #.we don't use "stub" as the method name because this
  # is already used by minitest.
  # (also, minitest's stub doesn't work here because it
  #  requires that the method already be defined)
  #
  def self.tmp(var_name, value, &block)
    var_name = var_name.to_s
    original = data[var_name]
    data[var_name] = value
    yield
    data[var_name] = original
  end

  #
  # Permit aliases in yaml files. In ruby 3+, aliases are no longer allowed by default.
  # see https://bugs.ruby-lang.org/issues/17866
  if Gem::Version.new(Psych::VERSION) >= Gem::Version.new('4.0.0')
    def self.load_yaml_file(filepath)
      YAML.load_file(filepath, aliases: true)
    end
  else
    def self.load_yaml_file(filepath)
      YAML.load_file(filepath)
    end
  end

  private_class_method :load_yaml_file
end
