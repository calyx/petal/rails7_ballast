Gem::Specification.new do |spec|
  spec.name        = "theme"
  spec.version     = "0.1.0"
  spec.authors     = ["elijah"]
  spec.homepage    = "https://0xacab.org/calyx/gems/cuttlefish_themer"
  spec.summary     = "Cuttlefish Themer"
  spec.description = "Theme engine for Ruby on Rails"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = ""

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "zeitwerk"
  spec.add_dependency "rails"
  spec.add_dependency "sprockets-rails"
  spec.add_dependency "sassc-rails"
  spec.add_dependency "haml-rails"
  spec.add_dependency "bootstrap"
  spec.add_dependency "nest_helper"
end
