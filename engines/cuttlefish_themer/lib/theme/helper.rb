module Theme
  module Helper
    protected

    def theme
      controller.theme
    end

    def theme_sidebar_link(active:, path:, label:, icon:)
      active = active.is_a?(Hash) ? self.active(active) : active
      link_to(path, class: "nav-link #{active}") do
        icon(icon, content_tag(:span, label).html_safe)
      end
    end

    def theme_sidebar_section(label:, icon:)
      content_tag(:div, class: 'section') {
        icon(icon, content_tag(:span, label))
      } + content_tag(:div, '', class: 'section-rule')
    end

    def theme_favicon
      if theme.favicon
        content_tag(:link, '', rel: "icon", type: "image/png", href: image_path(theme.favicon))
      end
    end

    def theme_variables
      content_tag(:style) do
        "body {\n#{css_variable_definitions}}\n"
      end
    end

    def one_line(&block)
      haml_concat capture_haml(&block).gsub("\n", '').gsub('\\n', "\n")
    end

    def content_if_exists(partial, flags={})
      if content_for?(partial)
        content_for(partial)
      elsif lookup_context.template_exists?(partial, 'layouts', true)
        render "layouts/#{partial}"
      else
        ""
      end
    end

    #
    # loads an application's 'layouts/theme' partial, allow it to
    # define variables and content blocks.
    #
    def load_custom_theme
      if lookup_context.template_exists?('theme', 'layouts', true)
        render 'layouts/theme'
      end
      nil
    end

    private

    def css_variable_definitions
      theme.keys.filter_map {|key|
        next if key.ends_with?('_text') || theme[key].nil?
        value = if key.ends_with?('_url')
          "url(%s)" % image_path(theme[key])
        else
          theme[key]
        end
        css_key = key.gsub('_', '-')
        "--#{css_key}: #{value};"
      }.join("\n")
    end

  end
end