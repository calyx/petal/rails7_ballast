module Theme
  class Configuration < HashWithIndifferentAccess
    def method_missing(arg)
      fetch(arg, nil)
    end
    def apply(**args)
      merge!(**args)
    end
    def apply_defaults(**args)
      reverse_merge!(**args)
    end
  end
end
