Cuttlefish Theme Base
===============================

This a gem for use in Ruby on Rails applications to provide
a theme for the application.

You can use this gem directly, using configuration options to modify the
appearance, or you can fork the gem and modify the asset files.

This theme base is built using:

* Bootstrap 5
* SCSS
* HAML
* Sprockets
* nest_helper

Installation
----------------------------------------

`Gemfile`:

    gem 'cuttlefish_themer', path: 'engines/cuttlefish_themer'

`app/controllers/application_controller.rb`:

    class ApplicationController < ActionController::Base
      layout 'theme/application'
    end

Custom Installation
----------------------------------------

`app/assets/stylesheets/application.scss`:

    @import "theme/style";

`app/controllers/application_controller.rb`:

    class ApplicationController < ActionController::Base
      layout 'application'
    end

`app/views/layout/application`

    - content_for :sidebar do
       = render 'layouts/sidebar' # or whatever content you want for the side bar
    = render template: 'layouts/theme/admin'

Configuration
------------------------------------------

A theme variables are loaded from three places, in order:

1. global defaults in `cuttlefish_themer/lib/theme/default.rb`
2. app defaults in `config/initializers/theme.rb`
3. per-request variable overrides via the `theme` object.

An example `config/initializers/theme.rb`

    Theme.config.apply({
      content_bg: 'green',
    })

You can set variables for an action or controller:

    class ApplicationController < ActionController::Base
      before_filter :apply_theme
      private
      def apply_theme
        theme.apply(
          content_bg: 'red',
        )
      end
    end

Reference
-------------------------------------------

CSS Variables: https://getbootstrap.com/docs/5.0/customize/css-variables/

Sass Variables: https://github.com/twbs/bootstrap-rubygem/blob/master/assets/stylesheets/bootstrap/_variables.scss
