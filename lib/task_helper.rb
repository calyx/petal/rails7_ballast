require 'open3'

class TaskHelper
  attr_reader :errors

  def colorize(str, fg=nil, bg=nil, mode=nil)
    colors = {black:0,red:1,green:2,yellow:3,blue:4,magenta:5,cyan:6,white:7}
    fg_code = (colors[fg] || 9) + 30
    bg_code = (colors[bg] || 9) + 40
    mode_code = mode == :bold ? 1 : 0
    "\033[#{mode_code};#{fg_code};#{bg_code}m#{str}\033[0m"
  end

  def bad(*str)
    puts "%s: %s" % [colorize("BAD", :red, nil, :bold), str.join('. ')]
  end

  def ok(*str)
    puts "%s: %s" % [colorize("OK", :green, nil, :bold), str.join('. ')]
  end

  def print_errors
    bad(errors)
  end
  
  def stash(&block)
    stashed = 0 == run_quiet('git', 'stash', eat: "No local changes to save")
    yield
    if stashed
      run_quiet('git', 'stash', 'pop', eat: "No stash entries found.")
    end
  end

  INDENT = "    "

  def run(*cmd)
    exit_status = -1
    puts("%s %s" % [colorize(cmd[0], :blue, nil, :bold), cmd[1..].join(' ')])
    Open3.popen2e(*cmd) do |stdin, out, thread|
      while line = out.gets do
        puts("%s%s" % [INDENT, line])
      end
      exit_status = thread.value.exitstatus.to_i
    end
    if exit_status != 0
      bad("exited with code %s" % exit_status)
      exit exit_status
    end
  end

  def run_quiet(*cmd, eat:[])
    eat = [eat] unless eat.respond_to?(:include)
    exit_status = -1
    output = []
    puts("%s %s" % [colorize(cmd[0], :blue, nil, :bold), cmd[1..].join(' ')])
    Open3.popen2e(*cmd) do |stdin, out, thread|
      while line = out.gets do
        line.strip!
        unless eat.include?(line)
          output << line.strip
        end
      end
      exit_status = thread.value.exitstatus.to_i
    end
    if exit_status != 0 && output.any?
      bad("exited with code %s" % exit_status)
      output.each do |line|
        puts("%s%s" % [INDENT, line])
      end
      exit exit_status
    end
    return exit_status
  end
end
