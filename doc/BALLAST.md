# A foundation for your Rails app

Ballast is the term for the gravel that sits under a train track. This project provides the foundation for you to build a Ruby on Rails app on top of, in a way that encourages you to write modular applications where everything is an engine.

Rails 7 Ballast is rather opinionated:

* HAML for views https://haml.info/
* SASS for styles https://sass-lang.com/
* Bootstrap 5 for CSS https://getbootstrap.com/
* Sprockets for assets https://guides.rubyonrails.org/asset_pipeline.html

* Specific theme format
* Included set of helpers

# Getting started

Start by cloning rails7_ballast as the start of your app:

    git clone --origin upstream_ballast https://0xacab.org/calyx/petal/rails7_ballast.git my_app_name

Then, add your upstream origin for the app:

    git remote add origin git@gitlab.com/me/myapp_name.git

Push this to the origin:

    git push -u origin main

# Gemfile

The default gems used by Ballast are loaded in `Gemfile`, which you are not intended
to modify. To load custom gems for your application, modify `Gemfile.rb` instead
of `Gemfile`.

To alter the variable defined in `Gemfile` without modifying `Gemfile`, create a file
`Gemfile.conf` (evaluated as ruby code).

# Development patterns

**Getting an updated rails7_ballast**

When there are changes to the rails7_ballast foundation, then you run these commands to base your app on the latest:

    git checkout main
    git fetch upstream_ballast main
    git rebase upstream_ballast/main
    git push

# Managing Engines

Ballast uses git subtree to help you manage many engines and gems which are potentially tightly coupled with the code.

Most of the time, when you want a gem you simply add it so your Gemfile. However, sometimes the engines or gems that are part of your app are just different modules of the same app, modules that you might possibly become shared by other apps eventually.

In this later case, it can be cumbersome to write your application modules as completely stand alone gem engines. Ballast helps manage this by using git subtree to allow you to import and update engines with their own upstream git repos while keeping all the code local so you can easily modify and create pull requests from your changes.

## Adding a new engine

* Step 1: create the file `engines/engines.yml`. Use `engines/default.yml` as an example, but don't modify `default.yml`. 
* Step 2: run `bin/engines pull`

## Updating an engine

* Step 1: Specify the 'push' attribute in `engines.yml`.
* Step 2: Modify the code and commit changes as you normally would.
* Step 3: run `bin/engines push`

## Installing engine migrations

If any of your engines have new migrations, run this to install them in the application itself:

    bin/rails railties:install:migrations
