class ApplicationController < ActionController::Base
  layout "theme/application"

  if defined?(AuthenticatedApp)
    include AuthenticatedAppConcern
  else
    def current_user
    end
    helper_method :current_user
  end
end
