require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

require 'conf'
Conf.load(
  File.expand_path('../defaults.yml', __FILE__),
  File.expand_path('../config.yml', __FILE__)
)

module RailsBallast
  class Application < Rails::Application

    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    # https://guides.rubyonrails.org/configuring.html

    config.load_defaults 7.0
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
