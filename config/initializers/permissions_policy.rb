#
# https://guides.rubyonrails.org/security.html#feature_policy_header
# https://github.com/rails/rails/blob/main/actionpack/lib/action_dispatch/http/permissions_policy.rb
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy
#
# There is, sadly, no way (yet) to turn all these off by default, so we have to name each one.
#
Rails.application.config.permissions_policy do |f|
  skip = [:build, :directives, :vr, :speaker, :vibrate]
  (f.methods - skip - Object.methods).each do |directive|
    f.send(directive, :none)
  end
end